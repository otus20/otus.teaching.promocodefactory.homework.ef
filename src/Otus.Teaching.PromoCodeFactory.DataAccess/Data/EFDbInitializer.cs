﻿using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
   public class EfDbInitializer
      : IDbInitializer
   {
      private readonly DataContext _dataContext;

      public EfDbInitializer(DataContext dataContext)
      {
         _dataContext = dataContext;
      }

      public void InitializeDb()
      {
         //_dataContext.Database.EnsureDeleted();
         var created = _dataContext.Database.EnsureCreated();

         if (!created)
         {
            var initialized = _dataContext.Roles.Any();

            if (initialized)
               return;
         }

         _dataContext.AddRange(FakeDataFactory.Roles);
         _dataContext.SaveChanges();

         _dataContext.AddRange(FakeDataFactory.Employees);
         _dataContext.SaveChanges();

         _dataContext.AddRange(FakeDataFactory.Preferences);
         _dataContext.SaveChanges();

         _dataContext.AddRange(FakeDataFactory.Customers);
         _dataContext.SaveChanges();
      }
   }
}