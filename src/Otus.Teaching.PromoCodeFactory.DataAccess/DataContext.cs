﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
   public class DataContext : DbContext
   {
      public DbSet<Employee> Employees { get; set; }
      public DbSet<Role> Roles { get; set; }
      public DbSet<Customer> Customers { get; set; }
      public DbSet<Preference> Preferences { get; set; }
      public DbSet<PromoCode> PromoCodes { get; set; }

      public DataContext()
      {
      }

      public DataContext(DbContextOptions<DataContext> options)
         : base(options)
      {
      }

      protected override void OnModelCreating(ModelBuilder modelBuilder)
      {
         // Employee
         modelBuilder.Entity<Employee>()
            .HasOne(x => x.Role)
            .WithMany()
            .HasForeignKey(x => x.RoleId);

         modelBuilder.Entity<Employee>()
            .Property(b => b.FirstName).HasMaxLength(200);

         modelBuilder.Entity<Employee>()
            .Property(b => b.LastName).HasMaxLength(200);

         modelBuilder.Entity<Employee>()
            .Property(b => b.Email).HasMaxLength(200);

         // Role
         modelBuilder.Entity<Role>()
            .Property(b => b.Name).HasMaxLength(200);

         modelBuilder.Entity<Role>()
            .Property(b => b.Description).HasMaxLength(200);

         // Customer
         modelBuilder.Entity<Customer>()
            .HasMany(x => x.Preferences)
            .WithOne(x => x.Customer)
            .HasForeignKey(x => x.CustomerId);

         modelBuilder.Entity<Customer>()
            .HasMany(x => x.Promocodes)
            .WithOne(x => x.Customer)
            .HasForeignKey(x => x.CustomerId);

         modelBuilder.Entity<Customer>()
            .Property(b => b.FirstName).HasMaxLength(200);

         modelBuilder.Entity<Customer>()
            .Property(b => b.LastName).HasMaxLength(200);

         modelBuilder.Entity<Customer>()
            .Property(b => b.Email).HasMaxLength(200);

         // Preference
         modelBuilder.Entity<Preference>()
            .Property(p => p.Name).HasMaxLength(200);

         // CustomerPreference
         modelBuilder.Entity<CustomerPreference>()
            .HasKey(bc => new { bc.CustomerId, bc.PreferenceId });

         // PromoCode
         modelBuilder.Entity<PromoCode>()
            .HasOne(p => p.PartnerManager)
            .WithMany()
            .HasForeignKey(x => x.PartnerManagerId);

         modelBuilder.Entity<PromoCode>()
            .HasOne(p => p.Preference)
            .WithMany()
            .HasForeignKey(x => x.PreferenceId);
         
         modelBuilder.Entity<PromoCode>()
            .Property(p => p.Code).HasMaxLength(200);

         modelBuilder.Entity<PromoCode>()
            .Property(p => p.ServiceInfo).HasMaxLength(200);

         modelBuilder.Entity<PromoCode>()
            .Property(p => p.ServiceInfo).HasMaxLength(200);
         
         modelBuilder.Entity<PromoCode>()
            .Property(p => p.PartnerName).HasMaxLength(200);
      }
   }
}