using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
   public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
   {
      public DataContext CreateDbContext(string[] args)
      {
         var optionsBuilder = new DbContextOptionsBuilder<DataContext>();
         optionsBuilder.UseSqlite(@"Filename=..\Otus.Teaching.PromoCodeFactory.WebHost\PromoCodeFactoryDb.sqlite");

         return new DataContext(optionsBuilder.Options);
      }
   }
}