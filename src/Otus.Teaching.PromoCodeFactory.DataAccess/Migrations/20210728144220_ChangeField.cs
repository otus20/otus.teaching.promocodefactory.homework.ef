﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class ChangeField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AppliedPromocodesCount",
                table: "Employees",
                newName: "AppliedPromocodesCount2");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AppliedPromocodesCount2",
                table: "Employees",
                newName: "AppliedPromocodesCount");
        }
    }
}
