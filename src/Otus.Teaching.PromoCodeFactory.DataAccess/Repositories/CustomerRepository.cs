﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
   public class CustomerRepository : EfRepository<Customer>
   {
      public CustomerRepository(DataContext dataContext) : base(dataContext)
      {
      }

      public override async Task<Customer> GetByIdAsync(Guid id)
      {
         return await DataContext.Set<Customer>()
            .Include(x => x.Preferences)
            .ThenInclude(x => x.Preference)
            .Include(x => x.Promocodes)
            .FirstOrDefaultAsync(x => x.Id == id);
      }

      public override async Task DeleteAsync(Customer customer)
      {
         DataContext.Set<PromoCode>().RemoveRange(customer.Promocodes);
         DataContext.Set<Customer>().Remove(customer);
         await DataContext.SaveChangesAsync();
      }
   }
}