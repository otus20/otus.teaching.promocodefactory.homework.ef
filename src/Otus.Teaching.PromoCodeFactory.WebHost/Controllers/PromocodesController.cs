﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromoCodesController
        : ControllerBase
    {
       private readonly IRepository<PromoCode> _promoCodeRepository;
       private readonly IRepository<Preference> _preferenceRepository;
       private readonly IRepository<CustomerPreference> _customerPreferenceRepository;

       public PromoCodesController(IRepository<PromoCode> promoCodeRepository,
          IRepository<CustomerPreference> customerPreferenceRepository,
          IRepository<Preference> preferenceRepository,
          CustomerRepository customerRepository)
       {
          _promoCodeRepository = promoCodeRepository;
          _customerPreferenceRepository = customerPreferenceRepository;
          _preferenceRepository = preferenceRepository;
       }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
           var promoCodes = await _promoCodeRepository
              .GetAllAsync();

           var promoCodesResponse = promoCodes
              .Select(x =>
                 new PromoCodeShortResponse()
                 {
                    Id = x.Id,
                    BeginDate = x.BeginDate.ToString("yyyy-MM-ddTHH:mm:ss"),
                    EndDate = x.EndDate.ToString("yyyy-MM-ddTHH:mm:ss"),
                    Code = x.Code,
                    PartnerName = x.PartnerName,
                    ServiceInfo = x.ServiceInfo
                 })
              .ToList();

           return Ok(promoCodesResponse);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением.
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
           var preferences = await _preferenceRepository.GetWhere(x => request.Preference == x.Name);
           var preference = preferences.FirstOrDefault();

           if (preference == null)
              return BadRequest();

           var customerPreferences = await _customerPreferenceRepository
              .GetWhere(x => x.PreferenceId == preference.Id);

           foreach (var customerPreference in customerPreferences)
           {
              var promoCode = new PromoCode
              {
                 Id = Guid.NewGuid(),
                 Code = request.PromoCode,
                 PartnerName = request.PartnerName,
                 ServiceInfo = request.ServiceInfo,
                 PreferenceId = preference.Id,
                 CustomerId = customerPreference.CustomerId
              };

              await _promoCodeRepository.AddAsync(promoCode);
           }

           return Ok();
        }
    }
}